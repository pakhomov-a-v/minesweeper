import { ICell, INeighborsInfo } from './interfaces';
import { adjAddOns } from './constants';
import { CellStatus } from './enums';
import * as cloneDeep from 'lodash/cloneDeep';

/**
 * Генератор случайных целых чисел в заданом диапазоне
 *
 * @param {number} max До какого значения может быть случайное число
 * @return {number} Случайное целое число
 */
const randomInteger = (max): number => {
    const rand = Math.random() * max;
    return Math.floor(rand);
};

/**
 * Получение ячеек не выходящих за границы игрового поля при обходе вокруг заданной ячейки
 *
 * @param {number[]} dimensions Размеры игрового поля
 * @param {number[][]} adjAddOns Относительные координаты соседних ячеек
 * @param {ICell} currentCell Текущая ячейка
 *
 * @return {number[][]} Массив координат соседних ячеек, которые не выходят за границы
 * игрового поля.
 */
export const getAdjCells = (
    dimensions: number[],
    adjAddOns: number[][],
    currentCell: ICell,
): number[][] => {
    const adjCoords = [];
    adjAddOns.map((addOn) => {
        const newY = currentCell.y + addOn[0];
        const newX = currentCell.x + addOn[1];
        if (newY >= 0 && newY < dimensions[0] && newX >= 0 && newX < dimensions[1]) {
            adjCoords.push([newY, newX]);
        }
    });
    return adjCoords;
};

/**
 * Получение поля заданных размеров
 *
 * @param {number} x Ширина поля
 * @param {number} y Высота поля
 *
 * @return {ICell[][]} Игровое поле
 */
export const getField = (x: number, y: number): ICell[][] => {
    const field = [];
    for (let i = 0; i < y; i++) {
        const row = [];
        for (let j = 0; j < x; j++) {
            row.push({
                x: j,
                y: i,
                cellStatus: CellStatus.EMPTY,
                uniqKey: `${j}.${i}`,
                isOpen: false,
                canClick: false,
                isEmptinessChecked: false,
            });
        }
        field.push(row);
    }
    return field;
};

/**
 * Получение заминированного поля
 *
 * @param {ICell[][]} field Игровое поле
 * @param {number} mineQuantity Высота поля
 *
 * @return {ICell[][]} Заминированное игровое поле
 */
export const getMinedField = (field: ICell[][], mineQuantity: number): ICell[][] => {
    const minedField: ICell[][] = field;
    const y = field.length;
    const x = field[0].length;

    while (mineQuantity != 0) {
        const rndX = randomInteger(x);
        const rndY = randomInteger(y);
        const hasBomb = minedField[rndY][rndX].cellStatus;
        if (hasBomb === CellStatus.EMPTY) {
            mineQuantity--;
            minedField[rndY][rndX].cellStatus = CellStatus.BOMB;
        }
    }
    return minedField;
};

/**
 * Получение поля, на котором промаркированы ячейки, соседствующие с минами
 *
 * @param {ICell[][]} field Игровое поле
 *
 * @return {ICell[][]} Промаркированное игровое поле
 */
export const getMarkedField = (field: ICell[][]): ICell[][] => {
    const fieldSize = [field.length, field[0].length];
    const markedField = cloneDeep(field);

    for (let i = 0; i < fieldSize[0]; i++) {
        for (let j = 0; j < fieldSize[1]; j++) {
            const currentCell = markedField[i][j];
            if (currentCell.cellStatus !== CellStatus.BOMB) {
                const adjCoords = getAdjCells(fieldSize, adjAddOns, currentCell);
                let bombCounter = 0;
                adjCoords.forEach((coord) => {
                    if (markedField[coord[0]][coord[1]].cellStatus === CellStatus.BOMB) {
                        bombCounter++;
                    }
                });
                currentCell.cellStatus = bombCounter;
            }
        }
    }
    return markedField;
};

/**
 * Открытие поля в случае проигрыша
 *
 * @param {ICell[][]} field Игровое поле
 * @return {ICell[][]} Игровое поле в случае проигрыша
 */
export const getLooseField = (field: ICell[][]): ICell[][] => {
    return field.map((row: ICell[]) => {
        return row.map((cell: ICell) => {
            const newCell = { ...cell };
            newCell.isOpen = true;
            if (newCell.cellStatus === CellStatus.BOMB) {
                newCell.cellStatus = CellStatus.LOOSE;
            }
            return newCell;
        });
    });
};

/**
 * Открытие поля в случае выигрыша
 *
 * @param {ICell[][]} field Игровое поле
 * @return {ICell[][]} Игровое поле в случае выигрыша
 */
export const getWinField = (field: ICell[][]): ICell[][] => {
    return field.map((row: ICell[]) => {
        return row.map((cell: ICell) => {
            const newCell = { ...cell };
            newCell.isOpen = true;
            if (newCell.cellStatus === CellStatus.BOMB) {
                newCell.cellStatus = CellStatus.WIN;
            }
            return newCell;
        });
    });
};

/**
 * Открытие пустых ячеек
 *
 * @param {ICell[][]} field Игровое поле
 * @param {ICell[]} emptiesArr Массив пустых ячеек. В момент вызова состоит из одного элемента
 * @return {ICell[][]} Поле с открытыми пустышками (со статусом == 0)
 */
export const openEmptyCells = (field: ICell[][], emptiesArr: ICell[]): ICell[][] => {
    const newField: ICell[][] = cloneDeep(field);
    const newEmptiesArr: ICell[] = cloneDeep(emptiesArr);
    let outcomeEmptiesArr: ICell[] = [];

    // Перебираем массив пустышек с целью открытия текущей пустышки и добавления в массив пустышек
    // соседних пустышек
    newEmptiesArr.forEach((emptie) => {
        newField[emptie.y][emptie.x].isOpen = true;
        newField[emptie.y][emptie.x].isEmptinessChecked = true;

        // Получили массив координат соседних ячеек
        const fieldSize = [newField.length, newField[0].length];
        const initCell = newField[emptie.y][emptie.x];
        const neighboringCellsCoords = getAdjCells(fieldSize, adjAddOns, initCell);
        // Пробежались по этому массиву и посмотрели на соответствующие ячейки нового поля
        // Если данные ячейки пустые и не просмотренные, то поместили их координаты в новый
        // массив пустых координат
        neighboringCellsCoords.forEach((neighborCoord) => {
            const neighborCell = newField[neighborCoord[0]][neighborCoord[1]];
            if (neighborCell.isOpen === false && neighborCell.cellStatus !== CellStatus.BOMB) {
                neighborCell.isOpen = true;
                if (neighborCell.cellStatus === CellStatus.EMPTY) {
                    outcomeEmptiesArr.push(neighborCell);
                }
            }
        });
    });

    // Отфильтровали новый массив координат по признаку !isEmptinessChecked
    outcomeEmptiesArr = outcomeEmptiesArr.filter((cell) => {
        return !cell.isEmptinessChecked;
    });

    return outcomeEmptiesArr.length === 0 ? newField : openEmptyCells(newField, outcomeEmptiesArr);
};

/**
 * Открытие ячеек при клике на промаркированую ячейку
 *
 * @param {ICell[][]} field Игровое поле
 * @param {ICell} initCell Промаркированая ячейка
 *
 * @return {INeighborsInfo} Информация о соседних ячейках
 */
export const getNeighborsInfo = (field: ICell[][], initCell: ICell): INeighborsInfo => {
    const fieldSize = [field.length, field[0].length];
    const neighboringCellsCoords = getAdjCells(fieldSize, adjAddOns, initCell);
    let flags = 0;
    let bombsPlaced = 0;
    let mistakeCounter = 0;

    neighboringCellsCoords.map((neighbor) => {
        const cell = field[neighbor[0]][neighbor[1]];
        if (cell.isFlagged) {
            flags++;
        }
        if (cell.cellStatus === CellStatus.BOMB) {
            bombsPlaced++;
        }
        if (cell.isFlagged && cell.cellStatus != CellStatus.BOMB) {
            mistakeCounter++;
        }
    });

    if (flags >= bombsPlaced) {
        if (mistakeCounter > 0) {
            return {
                flags: flags,
                mines: bombsPlaced,
                haveMistakes: true,
            };
        }

        neighboringCellsCoords.map((neighbor) => {
            const cell = field[neighbor[0]][neighbor[1]];
            if (!cell.isFlagged && !cell.isOpen && cell.cellStatus != CellStatus.BOMB) {
                cell.isOpen = true;
            }
        });
    }
    return {
        flags: flags,
        mines: bombsPlaced,
        haveMistakes: false,
    };
};

/**
 * Проверка на выигрыш
 *
 * @param {ICell[][]} field Игровое поле
 * @param {number} bombsPlaced Количество бомб на поле
 * @return {boolean} Был выигрыш или нет
 */
export const checkWin = (field: ICell[][], bombsPlaced: number): boolean => {
    let bombsToOpen = bombsPlaced;

    field.map((row) => {
        row.map((cell) => {
            cell.cellStatus === CellStatus.BOMB && (cell.isOpen || cell.isFlagged)
                ? --bombsToOpen
                : null;
        });
    });

    return !bombsToOpen;
};
