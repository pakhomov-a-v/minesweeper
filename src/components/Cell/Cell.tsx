import * as React from 'react';
import './Cell.scss';
import { CellStatus } from '../../enums';

/**
 * Интерфейс клика
 *
 * @param {SyntheticEvent} e Обертка над нативным событием
 * @param {number} x Координата Х
 * @param {number} y Координата У
 */
interface IOnClick {
    (e: React.SyntheticEvent, x: number, y: number): void;
}

/**
 * Интерфейс свойств компонента
 *
 * @param {number} x Координата Х
 * @param {number} y Координата У
 * @param {number} cellStatus Состояние ячейки
 * @param {boolean} isOpen Открыта ячейка или нет
 * @param {boolean} isFlagged Помечена ячейка флажком или нет
 * @param {function} onClick Обработчик клика на ячейке
 * @param {boolean} isEmptinessChecked
 */
interface IProps {
    x: number;
    y: number;
    cellStatus: number;
    isOpen: boolean;
    isFlagged: boolean;
    onClick: IOnClick;
    isEmptinessChecked?: boolean;
}

class Cell extends React.PureComponent<IProps> {
    /**
     * Обработчик клика на клетке
     *
     * @param {SyntheticEvent} e Обертка над нативным событием
     */
    clickOnCellHandler = (e: React.SyntheticEvent): void => {
        this.props.onClick(e, this.props.x, this.props.y);
    };

    /**
     * Отрисовка одиночной ячейки
     *
     * @return {ReactFragment} Ячейка
     */
    drawField = (): React.ReactFragment => {
        const cellStatus = this.props.cellStatus;
        // Ячейка открыта
        if (this.props.isOpen) {
            switch (cellStatus) {
                case CellStatus.EMPTY:
                    return <div className="openedEmptyCell"> </div>;

                case CellStatus.BOMB:
                    return (
                        <div className={`c${cellStatus} centeringWrapper`}>
                            <img
                                className="centeredImg"
                                src={`img/9.svg`}
                                onClick={this.clickOnCellHandler}
                                onContextMenu={this.clickOnCellHandler}
                                alt="Bomb"
                            />
                        </div>
                    );
                case CellStatus.LOOSE:
                    return (
                        <div className={`c${cellStatus} centeringWrapper`}>
                            <img
                                className="centeredImg"
                                src={`img/9.svg`}
                                onClick={this.clickOnCellHandler}
                                onContextMenu={this.clickOnCellHandler}
                                alt="Bomb"
                            />
                        </div>
                    );
                case CellStatus.WIN:
                    return (
                        <div className={`c${cellStatus} centeringWrapper`}>
                            <img
                                className="centeredImg"
                                src={`img/9.svg`}
                                onClick={this.clickOnCellHandler}
                                onContextMenu={this.clickOnCellHandler}
                                alt="Bomb"
                            />
                        </div>
                    );
                default:
                    return (
                        <div
                            className={`c${cellStatus} d-flex flex-wrap align-content-center justify-content-center`}
                            onClick={this.clickOnCellHandler}
                            onContextMenu={this.clickOnCellHandler}
                        >
                            {`${cellStatus}`}
                        </div>
                    );
            }
            // Ячейка закрыта и помечена флажком
        } else if (this.props.isFlagged) {
            return (
                <div className="closedCell flag centeringWrapper">
                    <img
                        className="centeredImg"
                        src={`img/-1.svg`}
                        onClick={this.clickOnCellHandler}
                        onContextMenu={this.clickOnCellHandler}
                        alt="Flagged cell"
                    />
                </div>
            );
            // Ячейка закрыта и не помечена флажком
        } else
            return (
                <div
                    className="closedCell innerShadow"
                    onClick={this.clickOnCellHandler}
                    onContextMenu={this.clickOnCellHandler}
                >
                    {' '}
                </div>
            );
    };

    render(): React.ReactFragment {
        return (
            <React.Fragment>
                <div>{this.drawField()}</div>
            </React.Fragment>
        );
    }
}

export default Cell;
