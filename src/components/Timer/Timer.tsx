import * as React from 'react';
import { connect } from 'react-redux';
import { IActions, IActionType, IDispatchProps, IGlobalStore } from '../../interfaces';
import { Dispatch } from 'redux';
import { Actions } from '../../actions/actions';
import './Timer.scss';

/**
 * Интерфейс свойств компонента
 *
 * @param {boolean} isTimerRun Тикают-ли часики
 * @param {number} timer Стор Показания таймера
 * @param {IActions} actions Экшны приложения
 */
interface IProps {
    isTimerRun: boolean;
    timer: number;
    actions: IActions;
}

class Timer extends React.PureComponent<IProps> {
    /**
     * Отрисовка таймера
     *
     * @return {ReactFragment} Элемент-таймер
     */
    renderTimer = (): React.ReactFragment => {
        if (this.props.isTimerRun) {
            this.props.actions.increaseTimer();
        }

        return (
            <div className="innerShadow timer d-flex flex-wrap align-content-center justify-content-center">
                {this.props.timer.toFixed(1)}
            </div>
        );
    };

    render(): React.ReactFragment {
        return <React.Fragment>{this.renderTimer()}</React.Fragment>;
    }
}

const mapStateToProps = (store: IGlobalStore): {} => {
    return {
        isTimerRun: store.isTimerRun,
        timer: store.timer,
    };
};

const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatchProps => ({
    actions: new Actions(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
