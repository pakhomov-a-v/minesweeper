import * as React from 'react';
import Timer from '../Timer/Timer';
import MineCounter from '../MineCounter/MineCounter';
import Field from '../Field/Field';
import GameMode from '../GameMode/GameMode';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

class App extends React.PureComponent {
    render(): React.ReactFragment {
        return (
            <div className="container">
                <div>&nbsp;</div>

                <div className="row">
                    <div className="col">&nbsp;</div>
                    <Timer />
                    <div className="emptyBlock">&nbsp;</div>
                    <MineCounter />
                    <div className="col">&nbsp;</div>
                </div>

                <div className="row">
                    <div className="col">&nbsp;</div>
                    <GameMode />
                    <div className="col">&nbsp;</div>
                </div>

                <div className="row  d-flex flex-wrap align-content-center justify-content-center">
                    <Field />
                </div>
            </div>
        );
    }
}

export default App;
