import * as React from 'react';
import { connect } from 'react-redux';
import { IGlobalStore } from '../../interfaces';
import './MineCounter.scss';

/**
 * Интерфейс свойств компонента
 *
 * @param {number} bombsLeft Количество оставшихся бомб
 */
export interface IMineCounterProps {
    bombsLeft: number;
}

class MineCounter extends React.PureComponent<IMineCounterProps> {
    render(): React.ReactFragment {
        return (
            <React.Fragment>
                <div className="innerShadow mineCounter d-flex flex-wrap align-content-center justify-content-center">
                    {this.props.bombsLeft}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (store: IGlobalStore): IMineCounterProps => {
    return {
        bombsLeft: store.bombsLeft,
    };
};

export default connect(mapStateToProps)(MineCounter);
