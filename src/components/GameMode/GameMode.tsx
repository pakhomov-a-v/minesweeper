import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IActions, IActionType, IDispatchProps } from '../../interfaces';
import { Actions } from '../../actions/actions';
import './GameMode.scss';

/**
 * Интерфейс свойств компонента
 *
 * @param {IActions} actions Экшны приложения
 */
interface IProps {
    actions: IActions;
}

class GameMode extends React.PureComponent<IProps> {
    /**
     * Обработчик клика на кнопке "Новичок"
     */
    handleNewbie = (): void => {
        this.props.actions.clickOnNewbie();
    };

    /**
     * Обработчик клика на кнопке "Любитель"
     */
    handleAmateur = (): void => {
        this.props.actions.clickOnAmateur();
    };

    /**
     * Обработчик клика на кнопке "Профи"
     */
    handleProf = (): void => {
        this.props.actions.clickOnProf();
    };

    render(): React.ReactFragment {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">&nbsp;</div>
                    <div className="innerShadow gameMode" onClick={this.handleNewbie}>
                        Новичок
                    </div>
                    <div className="innerShadow gameMode" onClick={this.handleAmateur}>
                        Любитель
                    </div>
                    <div className="innerShadow gameMode" onClick={this.handleProf}>
                        Профи
                    </div>
                    <div className="col">&nbsp;</div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatchProps => ({
    actions: new Actions(dispatch),
});

export default connect(null, mapDispatchToProps)(GameMode);
