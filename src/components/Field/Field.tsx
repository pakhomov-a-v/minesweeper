import * as React from 'react';
import Cell from '../Cell/Cell';
import { connect } from 'react-redux';
import { IActions, ICell, IGlobalStore, IActionType, IDispatchProps } from '../../interfaces';
import { Actions } from '../../actions/actions';
import { Dispatch } from 'redux';

/**
 * Интерфейс свойств компонента
 *
 * @param {IGlobalStore} globalStore Стор приложения
 * @param {IActions} actions Экшны приложения
 */
interface IProps {
    field: ICell[][];
    actions: IActions;
}

class Field extends React.PureComponent<IProps> {
    /**
     * Обработчик клика на клетке
     *
     * @param {SyntheticEvent} e Обертка над нативным событием
     * @param {number} x Координата Х ячейки
     * @param {number} y Координата У ячейки
     */
    clickOnCellHandler = (e: React.SyntheticEvent<HTMLElement>, x: number, y: number): void => {
        e.preventDefault();
        if (e.type === 'click') {
            this.props.actions.leftClick(x, y);
        } else if (e.type === 'contextmenu') {
            this.props.actions.rightClick(x, y);
        }
    };

    /**
     * Отрисовка игрового поля
     *
     * @return {ReactFragment} Игровое поле
     */
    renderAllCells = (): React.ReactFragment => {
        const cells: ICell[][] = this.props.field;
        return (
            <div>
                <div className="mainShadow">
                    {cells.map((row: ICell[]) => (
                        <div key={row[0].uniqKey}>
                            <div className="row">
                                {row.map((cell) => (
                                    <Cell
                                        x={cell.x}
                                        y={cell.y}
                                        key={cell.uniqKey}
                                        isOpen={cell.isOpen}
                                        cellStatus={cell.cellStatus}
                                        onClick={this.clickOnCellHandler}
                                        isFlagged={cell.isFlagged}
                                    />
                                ))}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    };

    render(): React.ReactFragment {
        return (
            <React.Fragment>
                <div>{this.renderAllCells()}</div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (store: IGlobalStore): { field: ICell[][] } => {
    return {
        field: store.field,
    };
};

const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatchProps => ({
    actions: new Actions(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Field);
