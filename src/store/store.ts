import { compose, createStore } from 'redux';
import { reducer } from '../Reducers/reducer';

const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose || compose;
export const store = createStore(reducer, composeEnhancers());
