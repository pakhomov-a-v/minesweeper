import {
    checkWin,
    getAdjCells,
    getField,
    getLooseField,
    getMarkedField,
    getMinedField,
    getWinField,
    getNeighborsInfo,
    openEmptyCells,
} from '../utils';
import { IActionType, ICell, IGlobalStore } from '../interfaces';
import { ActionTypes, CellStatus } from '../enums';
import * as cloneDeep from 'lodash/cloneDeep';
import { adjAddOns } from '../constants';

/**
 * Получение начального состояния стора
 *
 * @param {number} width Ширина игрового поля
 * @param {number} height Высота игрового поля
 * @param {number} bombs Количество бомб на поле
 * @return {IGlobalStore} Начальное состояние стора
 */
const getInitState = (width = 8, height = 8, bombs = 10): IGlobalStore => {
    const initField = getField(width, height);
    const minedField = getMinedField(initField, bombs);
    const markedField = getMarkedField(minedField);

    return {
        field: markedField,
        fieldWidth: width,
        fieldHeight: height,
        totalCells: width * height,
        bombsPlaced: bombs,
        bombsLeft: bombs,
        isTimerRun: false,
        timer: 0,
    };
};

export const reducer = (state = getInitState(), action: IActionType): IGlobalStore => {
    switch (action.type) {
        // Постановка/снятие флажка
        case ActionTypes.SWITCH_FLAG:
            let isWinByFlag = false;
            const newStateFlag = cloneDeep(state);
            const rightClickCell = newStateFlag.field[action.payload.y][action.payload.x];

            if (state.isTimerRun === true && rightClickCell.isOpen === false) {
                rightClickCell.isFlagged = !rightClickCell.isFlagged;
                rightClickCell.isFlagged ? --newStateFlag.bombsLeft : ++newStateFlag.bombsLeft;
            }

            isWinByFlag = checkWin(newStateFlag.field, newStateFlag.bombsPlaced);
            if (isWinByFlag) {
                newStateFlag.bombsLeft = 0;
                newStateFlag.isTimerRun = false;
                newStateFlag.field = getWinField(newStateFlag.field);
                return newStateFlag;
            }

            return newStateFlag;

        // Левый клик
        case ActionTypes.LEFT_CLICK:
            const newStateLClick = cloneDeep(state);
            const leftClickCell = newStateLClick.field[action.payload.y][action.payload.x];
            let isWinByLeftClick = false;

            // Можно-ли сделать ход на данной клетке?
            if (state.isTimerRun) {
                // Клик на открытой промаркированой ячейке
                if (leftClickCell.isOpen) {
                    const neighborsInfo = getNeighborsInfo(state.field, leftClickCell);
                    if (neighborsInfo.haveMistakes) {
                        newStateLClick.isTimerRun = false;
                        newStateLClick.field = getLooseField(newStateLClick.field);
                        return newStateLClick;
                    }

                    if (
                        leftClickCell.cellStatus > CellStatus.EMPTY &&
                        leftClickCell.cellStatus < CellStatus.BOMB &&
                        neighborsInfo.flags === neighborsInfo.mines
                    ) {
                        const fieldSize = [state.fieldWidth, state.fieldHeight];
                        const adjCoords = getAdjCells(fieldSize, adjAddOns, leftClickCell);
                        adjCoords.forEach((coords) => {
                            const neighborCell: ICell = newStateLClick.field[coords[0]][coords[1]];
                            if (
                                !neighborCell.isOpen &&
                                neighborCell.cellStatus != CellStatus.BOMB
                            ) {
                                neighborCell.isOpen = true;
                                if (neighborCell.cellStatus === CellStatus.EMPTY) {
                                    newStateLClick.field = openEmptyCells(newStateLClick.field, [
                                        neighborCell,
                                    ]);
                                }
                            }
                        });
                    }
                    isWinByLeftClick = checkWin(newStateLClick.field, newStateLClick.bombsPlaced);
                    if (isWinByLeftClick) {
                        newStateLClick.bombsLeft = 0;
                        newStateLClick.isTimerRun = false;
                        newStateLClick.field = getWinField(newStateLClick.field);
                        return newStateLClick;
                    }
                    return newStateLClick;
                } else {
                    newStateLClick.field[action.payload.y][action.payload.x].isOpen = true;
                    // Подрыв
                    if (leftClickCell.cellStatus === CellStatus.BOMB) {
                        newStateLClick.isTimerRun = false;
                        newStateLClick.field = getLooseField(newStateLClick.field);
                        return newStateLClick;
                    }
                    // Открытие ячейки
                    else if (leftClickCell.cellStatus === CellStatus.EMPTY) {
                        leftClickCell.isOpen = true;
                        leftClickCell.isEmptinessChecked = true;

                        const firstEmptyCell: ICell[] = [leftClickCell];
                        newStateLClick.field = openEmptyCells(state.field, firstEmptyCell);
                    }
                    isWinByLeftClick = checkWin(newStateLClick.field, newStateLClick.bombsPlaced);
                    // Победа
                    if (isWinByLeftClick) {
                        newStateLClick.bombsLeft = 0;
                        newStateLClick.isTimerRun = false;
                        newStateLClick.field = getWinField(newStateLClick.field);
                        return newStateLClick;
                    }
                    return newStateLClick;
                }
            }

            return newStateLClick;

        // Клик на элементе "Новичок"
        case ActionTypes.NEWBIE:
            let newStateNewbie = cloneDeep(state);
            newStateNewbie = getInitState();
            newStateNewbie.isTimerRun = true;
            newStateNewbie.timer = 0;
            return newStateNewbie;

        // Клик на элементе "Любитель"
        case ActionTypes.AMATEUR:
            let newStateAmateur = cloneDeep(state);
            newStateAmateur = getInitState(16, 16, 40);
            newStateAmateur.isTimerRun = true;
            newStateAmateur.timer = 0;
            return newStateAmateur;

        // Клик на элементе "Профи"
        case ActionTypes.PROF:
            let newStateProf = cloneDeep(state);
            newStateProf = getInitState(30, 16, 99);
            newStateProf.isTimerRun = true;
            newStateProf.timer = 0;
            return newStateProf;

        // Увеличение таймера
        case ActionTypes.INCREASE_TIMER:
            const newStateIncrease = { ...state };
            newStateIncrease.timer += 0.1;
            return newStateIncrease;

        default:
            return state;
    }
};
