import { Dispatch } from 'redux';
import { ActionTypes } from '../enums';

export class Actions {
    constructor(private dispatch: Dispatch) {}

    /**
     * Диспатчит левый клик
     *
     * @param {number} x Координата Х клика
     * @param {number} y Координата У клика
     */
    leftClick(x: number, y: number): void {
        this.dispatch({
            type: ActionTypes.LEFT_CLICK,
            payload: {
                y: y,
                x: x,
            },
        });
    }

    /**
     * Диспатчит постановку/снятие флажка
     *
     * @param {number} x Координата Х клика
     * @param {number} y Координата У клика
     */
    rightClick(x: number, y: number): void {
        this.dispatch({
            type: ActionTypes.SWITCH_FLAG,
            payload: {
                y: y,
                x: x,
            },
        });
    }

    /**
     * Диспатчит увеличение таймера
     */
    increaseTimer(): void {
        const lastTimer = window.setTimeout(() => {}, 10);
        for (let i = 0; i <= lastTimer; i++) {
            clearTimeout(i);
        }

        setTimeout(() => {
            this.dispatch({ type: ActionTypes.INCREASE_TIMER });
        }, 100);
    }

    /**
     * Диспатчит клик на элементе "Новичок"
     */
    clickOnNewbie(): void {
        this.dispatch({ type: ActionTypes.NEWBIE });
    }

    /**
     * Диспатчит клик на элементе "Любитель"
     */
    clickOnAmateur(): void {
        this.dispatch({ type: ActionTypes.AMATEUR });
    }

    /**
     * Диспатчит клик на элементе "Профи"
     */
    clickOnProf(): void {
        this.dispatch({ type: ActionTypes.PROF });
    }
}
