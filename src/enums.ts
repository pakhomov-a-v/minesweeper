/**
 * Типы экшенов
 *
 * SWITCH_FLAG - Постановка/снятие флажка
 * LEFT_CLICK - Левый клик на игровом поле
 * NEWBIE - Клик на элементе "Новичок"
 * AMATEUR - Клик на элементе "Любитель"
 * PROF - Клик на элементе "Профи"
 * INCREASE_TIMER - Увеличение таймера
 */
export enum ActionTypes {
    SWITCH_FLAG = 'SWITCH_FLAG',
    LEFT_CLICK = 'LEFT_CLICK',
    NEWBIE = 'NEWBIE',
    AMATEUR = 'AMATEUR',
    PROF = 'PROF',
    INCREASE_TIMER = 'INCREASE_TIMER',
}

/**
 * Статусы
 *
 * EMPTY - Пустая ячейка
 * BOMB - Ячейка с миной
 * CLOSED - Закрытая ячейка
 * LOOSE - Ячейка с миной на красном фоне (в случае проигрыша)
 * WIN - Ячейка с миной на зеленом фоне (в случае выигрыша)
 */
export enum CellStatus {
    EMPTY = 0,
    BOMB = 9,
    LOOSE = 11,
    WIN = 12,
}
