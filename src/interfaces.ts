import { Action } from 'redux';
import { Actions } from './actions/actions';

/**
 * Интерфейс стора
 *
 * @param {ICell[][]} field Игровое поле
 * @param {number} fieldWidth Ширина поля
 * @param {number} fieldHeight Высота поля
 * @param {number} totalCells Количество ячеек
 * @param {number} bombsPlaced Мин на поле
 * @param {number} bombsLeft Осталось мин
 * @param {number} timer Показания таймера
 * @param {boolean} isTimerRun Запущен таймер или нет
 */
export interface IGlobalStore {
    field: ICell[][];
    fieldWidth: number;
    fieldHeight: number;
    totalCells: number;
    bombsPlaced: number;
    bombsLeft: number;
    timer: number;
    isTimerRun: boolean;
}

/**
 * Интерфейс ячейки
 *
 * @param {number} x Координата Х ячейки
 * @param {number} y Координата У ячейки
 * @param {number} cellStatus Статус ячейки
 * @param {string} uniqKey Уникальный идентификатор ячейки (для map)
 * @param {boolean} isOpen Открыта ячейка или нет
 * @param {boolean} canClick Можно-ли кликнуть на ячейке
 * @param {boolean} isFlagged Отмечена ячейка флажком или нет
 * @param {boolean} isEmptinessChecked Проводилась проверка на отсутствие по соседству мин или нет
 */
export interface ICell {
    x: number;
    y: number;
    cellStatus: number;
    uniqKey: string;
    isOpen: boolean;
    canClick: boolean;
    isFlagged: boolean;
    isEmptinessChecked: boolean;
}

/**
 * Интерфейс координат
 *
 * @param {number} x Координата Х
 * @param {number} y Координата У
 */
interface ICoordinates {
    x: number;
    y: number;
}

/**
 * Интерфейс типов экшенов
 *
 * @param {string} type Тип экшена
 * @param {ICoordinates} payload Данные для редьюсера
 */
export interface IActionType extends Action {
    type: string;
    payload?: ICoordinates;
}

/**
 * Интерфейс экшенов
 *
 * @param {function} leftClick ЛКМ на игровом поле
 * @param {function} rightClick ПКМ на игровом поле
 * @param {function} clickOnNewbie Клик на элементе "Новичок"
 * @param {function} clickOnAmateur Клик на элементе "Любитель"
 * @param {function} clickOnProf Клик на элементе "Профи"
 * @param {function} increaseTimer Увеличение таймера
 */
export interface IActions {
    leftClick(x: number, y: number);
    rightClick(x: number, y: number);
    clickOnNewbie();
    clickOnAmateur();
    clickOnProf();
    increaseTimer();
}

/**
 * Интерфейс экшенов, обернутых в dispatch
 *
 * @param {Actions} actions Экшены, обернутые в dispatch
 */
export interface IDispatchProps {
    actions: Actions;
}

/**
 * Интерфейс, описывающий инфу о соседних ячейках
 *
 * @param{number} flags Количество ячеек, отмеченых флагами
 * @param{number} mines Количество мин
 * @param{boolean} haveMistakes Разница между количеством флагов и количеством мин
 * */
export interface INeighborsInfo {
    flags: number;
    mines: number;
    haveMistakes: boolean;
}
