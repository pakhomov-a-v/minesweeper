const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const multiConfig = (env) => {
    const isProd = env === 'dev';

    const resultConfig = {
        context: path.resolve(__dirname, './src'),
        entry: './index.tsx',
        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
        },
        output: {
            path: path.join(__dirname, '/dist'),
            filename: 'bundle.js',
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, 'css-loader'],
                    exclude: ['/node_modules'],
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
                    exclude: ['/node_modules'],
                },
                {
                    test: /\.tsx?$/,
                    loader: 'awesome-typescript-loader',
                    exclude: ['/node_modules'],
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: 'index.html',
            }),
            new CopyPlugin([
                {
                    from: path.resolve(__dirname, './src/misc/static'),
                    to: path.resolve(__dirname, 'dist'),
                },
                {
                    from: path.resolve(__dirname, './src/misc/img'),
                    to: path.resolve(__dirname, 'dist/img'),
                },
            ]),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css',
                ignoreOrder: false,
            }),
            new OptimizeCssAssetsPlugin({
                assetNameRegExp: /\.css$/g,
                cssProcessor: require('cssnano'),
                cssProcessorPluginOptions: {
                    preset: ['default', { discardComments: { removeAll: true } }],
                },
                canPrint: true,
            }),
        ],
        optimization: {
            minimize: true,
            minimizer: [new TerserPlugin()],
        },
    };

    if (!isProd) {
        resultConfig.devServer = {
            contentBase: path.join(__dirname, 'dist'),
            port: 8081,
            inline: true,
            stats: {
                cached: false,
            },
        };
        resultConfig.cache = false;
        resultConfig.devtool = 'inline-source-map';
    }

    return resultConfig;
};

module.exports = multiConfig;
